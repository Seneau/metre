import { KeycloakInstance } from 'keycloak-js';
import { Component, OnInit } from '@angular/core';
import { KeycloakSecurityService } from '../_helpers/keycloak-security.service';


@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {

  kc: KeycloakInstance ;

  constructor(private ks: KeycloakSecurityService) {
    this.kc = ks.kc;
  }

  ngOnInit(){
  }
 
}

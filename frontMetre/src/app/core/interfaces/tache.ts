export interface Tache {
    reference?:String;
    date?: Date;
    zone?: String;
    datePlannifed?:Date;
}

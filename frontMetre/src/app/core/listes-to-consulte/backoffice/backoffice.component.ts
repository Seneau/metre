import { environment } from './../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilsService } from '../../_helpers/utils.service';

@Component({
  selector: 'app-backoffice',
  templateUrl: './backoffice.component.html',
  styleUrls: ['./backoffice.component.scss']
})
export class BackofficeComponent implements OnInit {

  mockUrl = environment.mockdataUrl;
  liste;
  searchInput: string;
  statutInput: string = "";

  page = 1;
  pageSize = 6;
  
  constructor(private http: HttpClient, private router: Router, private utilesService: UtilsService) { }

  ngOnInit(): void {
    this.getListe();
  }

  getListe(){
    return this.http.get(environment.apiUrl+'/api/bon-interventions').subscribe(
      bi => {
        this.liste = bi,
        console.log(this.liste);
      }
    );
  }
}

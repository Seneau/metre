import { Component, OnInit } from '@angular/core';
// import { User } from 'src/app/core/interfaces/User';
// import { KeycloakSecurityService } from 'src/app/core/_helpers/keycloak-security.service';
import { Router } from '@angular/router';
import { User } from '../../core/interfaces/User';
import { KeycloakSecurityService } from '../../core/_helpers/keycloak-security.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  user: User;
  roleUser = '';
  constructor(private ks: KeycloakSecurityService, private router: Router) { }

  ngOnInit(): void {
    if (this.ks.kc.authenticated){
      this.ks.kc.loadUserInfo().then( (userData: User) => {
        // console.log(userData);

        this.user = userData;
        console.log(this.user);

      });
      this.roleUser = this.ks.getCurrentRoleUser();
    }
  }

  goHome(){
    return this.router.navigate(['']);
  }

}

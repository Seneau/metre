import { environment } from './../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilsService } from '../../_helpers/utils.service';
import { KeycloakSecurityService } from '../../_helpers/keycloak-security.service';
import { User } from '../../interfaces/User';

@Component({
  selector: 'app-metreur',
  templateUrl: './metreur.component.html',
  styleUrls: ['./metreur.component.scss']
})
export class MetreurComponent implements OnInit {

  mockUrl:string = environment.mockdataUrl;
  liste;
  user: User;
  searchInput: string;
  statutInput: string = "en cours";
  
  constructor(
    private ks: KeycloakSecurityService,
    private utilsService: UtilsService,
  ) { }

  ngOnInit(): void {
    this.getUser();
  }

  getUser(){
    if (this.ks.kc.authenticated){
      this.ks.kc.loadUserInfo().then( (userData: User) => {
        this.user = userData;
        this.getListe(this.user["matricule"]);
      });
    }
  }

  getListe(matricule){
    return this.utilsService.getBiByMatMetreur(matricule).subscribe(
      bi => {
        this.liste = bi
      }
    );
  }

}

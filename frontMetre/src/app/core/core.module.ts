import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { AccueilComponent } from './accueil/accueil.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid'; // fullcalendar plugin
import interactionPlugin from '@fullcalendar/interaction'; // fullcalendar plugin
import timeGrid from '@fullcalendar/timegrid'; // fullcalendar plugin
import { RouterModule } from '@angular/router';
import { PlanningComponent } from './planning/planning.component';
import { KanbanModule } from '@syncfusion/ej2-angular-kanban';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgbModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { FormulaireComponent } from './formulaire/formulaire.component';
import { ListesToConsulteComponent } from './listes-to-consulte/listes-to-consulte.component';
import { ListesValidatedComponent } from './listes-validated/listes-validated.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchPipe } from './search.pipe';
import { NgxCaptureModule } from 'ngx-capture';
import { MetreurComponent } from './listes-to-consulte/metreur/metreur.component';
import { BackofficeComponent } from './listes-to-consulte/backoffice/backoffice.component';
import { StatutInputPipe } from './statut-input.pipe';
import { ResponsableTechniqueComponent } from './accueil/responsable-technique/responsable-technique.component';
import { MetreComponent } from './accueil/metre/metre.component';
import { ListeMetreursComponent } from './liste-metreurs/liste-metreurs.component';
import { ReaffectationComponent } from './reaffectation/reaffectation.component';
import { ListeBiRtComponent } from './listes-to-consulte/liste-bi-rt/liste-bi-rt.component';
import { MetreurPipe } from './search/metreur.pipe';

FullCalendarModule.registerPlugins([ // FullCalendar plugins
  dayGridPlugin,
  interactionPlugin,
  timeGrid
]);

@NgModule({
  declarations: [AccueilComponent, PlanningComponent, FormulaireComponent, ListesToConsulteComponent, ListesValidatedComponent, SearchPipe, MetreurComponent, BackofficeComponent, StatutInputPipe, MetreComponent, ResponsableTechniqueComponent, ListeMetreursComponent, ReaffectationComponent, ListeBiRtComponent, MetreurPipe],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    FullCalendarModule,
    RouterModule,
    KanbanModule,
    DragDropModule,
    NgbModule,
    MatExpansionModule,
    MatIconModule,
    MatFormFieldModule,
    NgxDropzoneModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCaptureModule,
    NgbPaginationModule
  ],
  providers: [
    DatePipe,
    { provide: LOCALE_ID, useValue: 'fr-FR'}
  ],
  exports: [
    AccueilComponent,
    FullCalendarModule,
    FormulaireComponent
  ]
})
export class CoreModule { }

export interface Materiel {
    "id"?: number,
    "bordereau"?: string,
    "chapitre"?: string,
    "libelle"?: string,
    "reference"?: string,
    "nouveaux"?: string,
    "nsid"?: string,
    "prix"?: number,
    "prix196"?: number,
    "unite"?: number,
    "quantite"?: string,
    "idBonItervention"?: number
}

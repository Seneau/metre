import { FirstLetterPipe } from './first-letter.pipe';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [FirstLetterPipe],

  exports: [
    FirstLetterPipe
  ]
})
export class PipeModule { }

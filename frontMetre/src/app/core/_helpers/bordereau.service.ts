import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
// import { environment } from 'src/environments/environment';
import { Materiel } from '../interfaces/materiel';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BordereauService {
  
  constructor(private http: HttpClient) {}
  bordereaux: Materiel[];
  // fonction pour récupérer les bordereaux
  getBordereaux(): Observable<Materiel[]>{
    return this.http.get<Materiel[]>(environment.apiUrl + '/api/materiauxes').
    pipe(map(materiaux => {
      // on recupere l'ensensemble des matériaux dans le tableau bordereaux
      this.bordereaux = materiaux;
      // on déclare un tableau les différents catégories
      const categories=[];
      // on parcourt les matériaux récupérés depuis le serveur
      materiaux.forEach(materiel => {
        // on verifie si le catégories n'existe pas encore dans le tableau des catégories
        if(categories.indexOf(materiel.bordereau)==-1){
          // sinon on l'ajoute
          categories.push(materiel.bordereau);
        }
      });
      // on retourne le tableau des catégories
      return categories;
    }))
  }

  // fonction pour récuperer les chapitres d'un bordereau
  getChapitresByBordereau(nomBordereau: string): Materiel[]{
    // tableau où  on va metre les chapitres du bordereau
    const mat = [];
    // on filtre dans les materiaux ceux qui ont le bordereau donné
    // en paramètre
    const materiaux = this.bordereaux.filter(m => m.bordereau == nomBordereau);
    materiaux.forEach(element => {
      // on parcours le tableau et on vérifie si le tableau ne contient pas déja
      if(mat.indexOf(element.chapitre) == -1){
        // sinon on l'ajoute dans le tableau
        mat.push(element.chapitre);
      }
    });
    // on retourne le tableau de chapitres
    return mat;
  }

  // fonction pour récupérer les libellés d'un chapitre
  getLibellesByChapitre(nomChapitre: string): string[]{
    // tableau où on va mettre les libellés
      const libelles=[];
      // on filtre les matériaux qui ont comme chapitre celui donné en paramètre
      let materiaux = this.bordereaux.filter(m => m.chapitre == nomChapitre);
      // on parcourt le tableau et si le libelle n'existe pas encore dans le 
      // tableau mat on l'y ajoute
      materiaux.forEach(element => {
        if(libelles.indexOf(element.libelle)==-1){
          libelles.push(element.libelle);
        }
      });
      return libelles;
  }

  getReferenceOfLibelle(libelle: string){
    let materiel = this.bordereaux.filter(m => m.libelle == libelle);
    return materiel[0].reference;
  }

  // fonction permettant d'envoyer les bordereaux choisis par le user
  chooseBordereau(body: Materiel): Observable<any>{
    return this.http.post(environment.mockdataUrl+"bordereauChoisis", body);
  }

  // fonction permettant de récuperer les bordereaux coté backoffice et RT
  getBordereauxChoisis(idBonItervention: number): Observable<Materiel[]>{
    return this.http.get<Materiel[]>(environment.mockdataUrl+"bordereauChoisis").pipe(
      map(materiaux => {
        return materiaux = materiaux.filter(x => x.idBonItervention==idBonItervention)
      })
    )
  }
}

// solliciter la bd une seule fois

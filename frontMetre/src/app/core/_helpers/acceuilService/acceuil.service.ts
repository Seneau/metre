import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
// import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AcceuilService {

  private host = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getTaches(){
    return this.http.get(`${this.host}/api/bon-interventions`);
  }

}

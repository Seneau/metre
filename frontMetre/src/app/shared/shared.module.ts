import { PipeModule } from './../@pipe/pipe.module';
import { UserDropdownInnerComponent } from './user-dropdown-inner/user-dropdown-inner.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [HeaderComponent, NavbarComponent, UserDropdownInnerComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatIconModule,
    PipeModule
  ],
  exports: [
    HeaderComponent,
    NavbarComponent,
    UserDropdownInnerComponent
  ]
})
export class SharedModule { }

import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { KeycloakInstance } from 'keycloak-js';
declare var Keycloak: any;

@Injectable({providedIn: 'root'})
export class KeycloakSecurityService{
 public kc: KeycloakInstance;

   public async init(){
      this.kc = new Keycloak({
        url: environment.keycloak.issuer,
        realm: environment.keycloak.realm,
        clientId: environment.keycloak.clientId
      });

      await this.kc.init({
        onLoad: 'login-required',
        checkLoginIframe: false
      })
   }


   getCurrentRoleUser() {
    let roleUser = null;
    const allRoleUser = this.kc.realmAccess.roles;

    allRoleUser.forEach( (role) => {
      if (role.includes('ROLE_')) {
        roleUser = role.substr(5);
      }
    });

    return roleUser;
   }

   getCurrentRoleUsercomplet() {
    let roleUser = null;
    const allRoleUser = this.kc.realmAccess.roles;

    allRoleUser.forEach( (role) => {
      if (role.includes('ROLE_')) {
        roleUser = role;
      }
    });

    return roleUser;
   }
}

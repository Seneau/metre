import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-responsable-technique',
  templateUrl: './responsable-technique.component.html',
  styleUrls: ['./responsable-technique.component.scss']
})
export class ResponsableTechniqueComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit(): void {
    this.router.navigateByUrl('/listeToConsulte');
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { BonIntervention } from '../../../interfaces/BonIntervention';
// import { BonIntervention } from 'src/app/core/interfaces/BonIntervention';
// import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AcceuilRtService {

  url = environment.apiUrl;

  constructor(private http : HttpClient) { }

  getBIBySecteur(): Observable<BonIntervention[]>{
    return this.http.get<BonIntervention[]>(`${this.url}/`);
  }

  getAllUsers(): Observable<any>{
    return this.http.get<any>(`${environment.keycloak.issuer}admin/realms/${environment.keycloak.realm}/users`)
  }

}

import { Component, OnInit } from '@angular/core';
import { KeycloakInstance } from 'keycloak-js';
import { KeycloakSecurityService } from '../../core/_helpers/keycloak-security.service';
// import { KeycloakSecurityService } from 'src/app/core/_helpers/keycloak-security.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  backOffice = true;
  metre = false;
  role;
  kc: KeycloakInstance ;

  constructor(private ks: KeycloakSecurityService) {
    this.kc = ks.kc;
  }

  ngOnInit(): void {
    this.getRole()
  }

  getRole(){
    return this.role = this.ks.getCurrentRoleUsercomplet();
  }

  // backOfficeConnection(){
  //   this.backOffice = true;
  //   this.metre = false;
  // }

  // metreConnection(){
  //   this.backOffice = false;
  //   return this.metre = true;
  // }

}

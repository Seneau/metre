import { KeycloakSecurityService } from './keycloak-security.service';
import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  constructor( private router: Router, private ks: KeycloakSecurityService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const rolesUser = this.ks.kc.idTokenParsed;
    //console.log(rolesUser);


    // return ;
    if (!this.ks.kc.authenticated) {
      // logged in so return true
      this.ks.kc.login({
         redirectUri: window.location.origin + state.url,
       });
    }
    if (!this.ks.kc.hasRealmRole('ROLE_BACK_OFFICE') && !this.ks.kc.hasRealmRole('ROLE_METREUR') && !this.ks.kc.hasRealmRole('ROLE_RESPONSABLE_TECHNIQUE')) {
      // logged in so return true
      this.ks.kc.logout({
         redirectUri: window.location.origin + state.url,
       });
    }

    const requiredRoles = route.data.roles;

    if (!(requiredRoles instanceof Array) || requiredRoles.length === 0) {
       return true;
    }

    console.log(this.ks.kc.realmAccess);
    let returnResult = false;
    requiredRoles.forEach( roleReq => { if (this.ks.kc.hasRealmRole(roleReq))
      {
        returnResult = true;
    }});
    return returnResult;
   }
}

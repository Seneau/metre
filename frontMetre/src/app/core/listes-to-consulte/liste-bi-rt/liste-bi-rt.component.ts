import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
// import { environment } from 'src/environments/environment';
import { BonIntervention } from '../../interfaces/BonIntervention';
import { AcceuilRtService } from '../../_helpers/acceuilService/acceuil-rt/acceuil-rt.service';
import { UtilsService } from '../../_helpers/utils.service';

@Component({
  selector: 'app-liste-bi-rt',
  templateUrl: './liste-bi-rt.component.html',
  styleUrls: ['./liste-bi-rt.component.scss']
})
export class ListeBiRtComponent implements OnInit {

  mockUrl:string = environment.mockdataUrl;

  //Ici on y stock les bi
  liste: any = [];

  //Ce tableau recevra les métreurs du rt connecté
  metreur = [];

  //Ce tableau recevra les matricules du rt connecté
  matricules = [];

  //Input de filtre
  searchInput: string;
  statutInput: string = "";

  page = 1;
  pageSize = 6;

  constructor(
    private utilsService: UtilsService,
    private serviceRt : AcceuilRtService
  ) { }

  ngOnInit(): void {
    this.getMetreurs();
  }

  //Cette méthode recupére tous les métreurs du RT connecté
  getMetreurs(){
    this.serviceRt.getAllUsers().subscribe(
      users => {
        this.metreur = this.utilsService.getMetreurByMatriculeRT(users, 121217);
        this.boucle(this.metreur);
      }
    );
  }

  //Ici on fait un push du matricule des métreurs dans le tableau matricules[]
  boucle(array){
    array.forEach(index => {
      const element = index;
      this.matricules.push(element["attributes"]["matricule"][0]);
    });
    this.getBI();
  }

  //Cette fonction recupére les bons interventions des métreurs du rt connecté
  getBI(){
    this.matricules.forEach(index => {
      this.utilsService.getBiByMatMetreur(index).subscribe(
        (bi) => {
          bi.forEach(element => {
            this.liste.push(element)
          })
        });
    });
  }

  // by mock......................................................................................
  // getListe(){
  //   return this.http.get(this.mockUrl+"interventions").subscribe(
  //     bi => {
  //       this.liste = bi,
  //       console.log(this.liste)
  //     }
  //   );
  // }

}

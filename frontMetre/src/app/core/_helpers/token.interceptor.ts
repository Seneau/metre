import { KeycloakSecurityService } from './keycloak-security.service';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private ks: KeycloakSecurityService){}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({
      setHeaders: {
        'Content-Type' : 'application/json; charset=utf-8',
        Accept       : 'application/json',
        Authorization: `Bearer ${this.ks.kc.token}`,
      },
    });

    return next.handle(req);
  }
}

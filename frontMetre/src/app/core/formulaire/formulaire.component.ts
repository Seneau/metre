import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InterventionService } from '../_helpers/intervention.service';
import Swal from 'sweetalert2'
import { KeycloakSecurityService } from '../_helpers/keycloak-security.service';
import { KeycloakInstance } from 'keycloak-js';
import { BordereauService } from '../_helpers/bordereau.service';
import { Materiel } from '../interfaces/materiel';
import { NgxCaptureService } from 'ngx-capture';
import { BonIntervention } from '../interfaces/BonIntervention';
import { UtilsService } from '../_helpers/utils.service';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.scss']
})
export class FormulaireComponent implements OnInit {
  panelOpenState = false;
  panelOpenState2 = false;
  panelOpenState3 = false;
  panelOpenState4 = false;
  panelOpenState5 = false;
  panelOpenState6 = false;
  panelOpenState7 = false;
  // on ouvre le premier panel
  step = 0;
  // le formulaire du BI
  interventionForm: FormGroup;
  // si le métré est annulé ou non
  isMettreCanceled = false;
  // le role user connecté
  userRole: string;
  // l'instance du keycloak
  kc: KeycloakInstance;
  // les bordereaux
  categories: Materiel[];
  // stocker les chapitres choisis
  chapitres: Materiel[] = [];
  // stocker les libéllés choisis
  libelles: any[] = [];
  // variable incrémenté à chaque ajout d'un new bordereau
  ligne: number = 0;
  // tableau pour stocker les bordereaux
  mat: Materiel[];
  // var pour afficher ou non le bouton d'ajout
  showAddBtn: boolean = false;
  // var pour stocker le libellé lors du choix du bordereau
  libelle: string;
  // var pour afficher ou non l'input pour la quantité
  showQtInput = false;
  // variable pour récupérer la quantité donnée par le user lors du choix du bordereau
  quantite: string;
  // utilisé pour les lignes du tableau de bordereaux
  tab: Materiel[] = [];
  // variable pour stocker le BI courant
  bi: BonIntervention;
  // variable pour stocker l'erreur lors de update s'il y en a
  updateError: string;
  // variable pour stocker l'erreur lors de l'neregistrement des bordereaux
  registerBordereauError: string;
  // var pour garder les tableaux des bordereaux choisis par le user
  brxChoisis: Materiel[];
  // représente chaque matrériel choisi par le métreur
  materiel: Materiel = {"bordereau": "", chapitre: "", libelle: "", reference: ""};

  constructor(private ks: KeycloakSecurityService, private formBuilder: FormBuilder, private http: HttpClient, private activatedRouter: ActivatedRoute, private interventionSerive: InterventionService, private router: Router, private bordereauService: BordereauService, private utilsService: UtilsService) { 
    
  }
  id = this.activatedRouter.snapshot.params['id'];

  ngOnInit(): void {
    this.getMetreBordereaux();
    // on initialise le formulaire
    this.initForm();
    // on récupère le métré courant
    this.userRole = this.ks.getCurrentRoleUser();
    // on récupère le métré courant grace à son id 
    this.interventionSerive.getMetre(this.interventionForm, this.id).subscribe(
      (bi) => {
        // on le met dans notre variable bi
        this.bi = bi;
        // on désactive le formulaire si le backoffice ou RT qui s'est connecté
        // ou si le formulaire est envoyé
        this.interventionSerive.ActivateViewOnly(this.userRole, this.interventionForm, bi);
      },
      () => {
        this.utilsService.showErrorMsg("Désolé une erreur est survenue du serveur. Veuillez réessayer plus tard.")
      }
    )

    // recuperer les catégories des bordereaux
    this.bordereauService.getBordereaux().subscribe(
      (materiaux) => {
        // on met les matériaux dans la variable "categories"
        this.categories = materiaux;
      }
    )
  }

  // aller vers un step
  setStep(index: number) {
    this.step = index;
  }

  // aller dans le panel suivant
  nextStep() {
    this.step++;
  }

  // retourner dans le panel précédent
  prevStep() {
    this.step--;
  }

  // onChangeBgOnHover() {
  //   this.panelOpenState = true;
  // }

  // on initialise le formulaire du BI : interventionForm
  initForm() {
    this.interventionForm = this.formBuilder.group({
      // données pré remplies
      "uoSecteur": [{ value: '', disabled: true }],
      "residence": [{ value: '', disabled: true }],
      "nomClient": [{ value: '', disabled: true }],
      "prenomClient": [{ value: '', disabled: true }],
      "typePrestation": [{ value: '', disabled: true }],
      "referenceClient": [{ value: '', disabled: true }],
      "referenceBi": [{ value: '', disabled: true }],
      "dateDebut": [{ value: '', disabled: true }],
      "dateFin": [{ value: '', disabled: true }],
      "nomAgent": [{ value: '', disabled: true }],
      "prenomAgent": [{ value: '', disabled: true }],
      "adresse1": [{ value: '', disabled: true }],
      "adresse2": [{ value: '', disabled: true }],
      "presenceClient": [{ value: '', disabled: true }],
      "complementAdresse": [{ value: '', disabled: true }],
      "typeEspace": [{ value: '', disabled: true }],
      "commentaires": [{ value: '', disabled: true }],
      // génralités du métré
      "presenceClientByMetreur": ["false"],
      "enginsPrevoir": [],
      "besoinDebitPression": [],
      "autreCommentaires": [],
      // données du métré
      "extension": ["false"],
      "terassementClient": ["false"],
      "protectionIncendie": ["false"],
      "separationRaccordement": ["false"],
      "droitSuite": ["false"],
      "climatisationRefroidissementEau": ["false"],
      "diametreBranchement": [],
      "longueurBranchementCanalisation": [],
      "longueurBranchementChausse": [],
      "diametreCanalisationExistante": [],
      "natureCanalisationExistante": [],
      "profondeurCanalisationExistante": [],
      "codeService": [],
      "codetrocon": [],
      "codeQuartier": [],
      "codeTournee": [],
      "rang": [],
      "nombrePointEau": [],
      "debitInstantanee": [],
      "debitJournalier": [],
      "debitHoraire": [],
      // consignes de sécurité à prévoir
      "feuxTricolor": ["false"],
      "circulationAlterne": ["false"],
      "foncageChausse": ["false"],
      "croisementOuvrage": ["false"],
      "blindageTranche": ["false"],
      "trotoir": ["false"],
      "accotement": ["false"],
      "empietementSansReduction": ["false"],
      "empietementAvecReduction": ["false"],
      "autresConsignes": [],
      // description quantitative
      "bordereau": [],
      "motifAbandon": []
    })
  }

  // fonction permettant de completer les infos du métré
  // fonction exécutée à la soumission du formulaire
  onSubmitForm() {
    this.utilsService.showConfrimationMsg("Voulez-vous envoyer les données ?").then(
      (result) => {
        if (result.isConfirmed) {
          console.log("bi à envoyé");
          console.log(this.bi);
          this.interventionSerive.updateMetre(this.interventionForm, this.bi).subscribe(
            () => {
              // si le update passe on enregistre les bordereaux séléctionnées
              this.registerBordereau();
            },
            (error) => {
              // on récupere l'erreur lors du update s'il y'en a
              this.updateError = error;
            }
          )
          // s'il y'a une erreur lors de la modification du BI
          // ou lors de l'enregistrement des bordereaux on envoie un msg d'erreur
          if (this.updateError != null || this.registerBordereauError != null) {
            this.utilsService.showErrorMsg("Désolé une erreur est survenue du serveur. Veuillez réessayer plus tard.");
            // on fait un redirection vers la liste des demandes
            this.router.navigate(["/listeToConsulte"]);
          } else {
            // sinon on envoie un msg de succès
            this.utilsService.showSuccesMsg("Métré envoyé avec succès");
            this.router.navigate(["/listeToConsulte"]);
          }
        }
      }
    )
  }

  // on recupere les chapitres d'un bordereau
  getChapitresOfBordereau(nomBordereau: string) {
    // on cache le bouton d'ajout
    this.showAddBtn = false;
    // on cache le input de quantité
    this.showQtInput = false;
    // on réinitailise le tableau des chapitres et des libellés
    this.chapitres = [];
    this.libelles = [];
    // on récupére les chapitres du catégorie
    this.chapitres = this.bordereauService.getChapitresByBordereau(nomBordereau);

    this.materiel.bordereau = nomBordereau;
  }

  // fonction appélée quand on chosit un chapitre
  getLibellesOfChapitre(nomChapitre: string) {
    this.showAddBtn = false;
    this.showQtInput = false;
    // on reinitialise le tableau des libelles
    this.libelles = [];
    // les récupere les libellés du chapitre
    this.libelles =  this.bordereauService.getLibellesByChapitre(nomChapitre);
    // on modifie notre materiel
    this.materiel.chapitre = nomChapitre;
  }

  // récuperer la référence d'un libellé
  getReferenceOfLibelle(libelle: string) {
    // on récupère le libellé
    this.libelle = libelle;
    // on affiche le champs quantité
    this.showQtInput = true;

    this.materiel.libelle = libelle;
    this.materiel.reference = this.bordereauService.getReferenceOfLibelle(libelle);
  }

  // fonction appélée quand on clique sur "ajout" d'un bordereau
  // ajouter une ligne dans le tableau
  addBordereau() {
    // on crée un boolean pour tester que la ligne ne sera pas dupliquée dans le tableau
    let is_exist: boolean = false;
    // si le tableau contient une ligne
    if (this.tab.length > 0) {
      // on parcourt le tableau
      for (let i = 0; i < this.tab.length; i++) {
        // on recherche si y'a pas une ligne tab[i] identique à la ligne
        // qu'on souhaite créer : "ligne"
        if (this.tab[i] && this.tab[i].bordereau == this.materiel.bordereau && this.tab[i].chapitre == this.materiel.chapitre && this.tab[i].libelle == this.materiel.libelle) {
          // si c'est le cas la variable is_exist = true
          is_exist = true;
          // on lance une pour dire qu'il y a duplication
          Swal.fire({
            position: 'bottom-end',
            title: 'Duplication de ligne',
            showConfirmButton: false,
            timer: 1500
          })
        }
      }
    }
    if (!is_exist) {
      // si la ligne n'existe pas encore
      // on la créé dans le tableau Tab
      this.tab[this.ligne] = { "bordereau": this.materiel.bordereau, "chapitre": this.materiel.chapitre, "libelle": this.materiel.libelle, "reference": this.materiel.reference, "quantite": this.quantite };
      // on incrémente le nombre d'éléments dans le tab: le nbre de lignes
      this.ligne = this.ligne + 1;
    }
  }

  // supprimer une ligne du tableau
  removeBordereau(index: number) {
    // on supprime l'élement dans Tab
    this.tab.splice(index, 1);
    // on décremente d'une ligne
    this.ligne = this.ligne - 1;
  }

  // fonction appélée à chaque fois le user met à jour la quantité
  // lors du choix d'un bordereau
  getQt(value: string) {
    // on affecte la quantité du bordereau 
    this.quantite = value;
    // on affiche le bouton "ajout" du bordereau
    this.showAddBtn = true;
    // si la quantité est = à "0" ou null on chache le bouton d'ajout
    if (value == "0" || value == "") {
      this.showAddBtn = false;
    }
  }

  // fonction appelé lorsque le user change la quantité dans le tableau
  changeQt(value: string, index: number) {
    // value => nouvelle quantité
    // index => l'index du tableau
    // si la nouvelle valeur != 0 et !=vide on met à jour la quantité
    if (value != "0" && value != "") {
      this.tab[index].quantite = value;
    }
  }

  // fonction appelée pour annuler le métré
  cancelMetre() {
    // on affiche une damande de confirmation
    this.utilsService.showConfrimationMsg("Êtes-vous sûr(e) d'annuler le métré ?").then(
      (result) => {
        // si le user confirme
        if (result.isConfirmed) {
          // on met le motifAbandon à required
          this.interventionForm.addControl("motifAbandon", this.formBuilder.control("", [Validators.required]))
          // le boolean qui certifie que le métré est annulé est mis à true
          this.isMettreCanceled = true;
          // on ouvre le step=6, le expension panel pour entrer le motif
          this.step = 6;
        }
      }
    )
  }

  // fonction pour envoyer le motif de l'annulation du métré
  sendMotif() {
    this.interventionSerive.adandonMetre(this.bi, this.interventionForm.controls["motifAbandon"].value).subscribe(
      (bi) => {
        this.utilsService.showSuccesMsg("Métré abandonné");
        this.router.navigate(["/listeToConsulte"]);
      },
      () => {
        this.utilsService.showErrorMsg("Désolé, veuillez réssayer plus tard");
        this.router.navigate(["/listeToConsulte"]);
      }
    )
  }

  // enregistrer le bordereau choisi
  registerBordereau() {
    // pour parcout le tableau contenant les bordereaux choisis
    this.tab.forEach(bordereau => {
      // pour chaque bordereau on met à affecte l'id du BI courant à son idBonIntervention
      bordereau.idBonItervention = this.id;
      this.bordereauService.chooseBordereau(bordereau).subscribe(
        () => {},
        (error) => {
          // s'il y a une erreur on l'affecte dans la variable registerBordereauError
          this.registerBordereauError = error;
        }
      )
    });
  }

  // recuperer la liste des brdx choisis par le métreur coté backoffice et RT
  getMetreBordereaux() {
    // on recupere les bordereaux ayant l'id du BI
    this.bordereauService.getBordereauxChoisis(this.id).subscribe(
      (materiaux) => {
        // pour chaque materiel on le met dans le tableau brxChoisis
        this.brxChoisis = materiaux;
      },
      (error) => console.log(error)
      // on affiche l'erreur 
    )
  }

  // valider le métré coté backoffice
  validerMetre() {
    // on envoie une demande de confirmation
    this.utilsService.showConfrimationMsg("Êtes-vous sûr de valider ce métré ?").then(
      (result) => {
        if (result.isConfirmed) {
          // s'il y a confirmation, on envoie la requete de validation vers le serveur
          this.interventionSerive.validateMetre(this.bi).subscribe(
            () => {
              // succès : on envoie un msg de succès et on redirige vers
              // la liste des demandes
              this.utilsService.showSuccesMsg("Métré validé");
              this.router.navigate(['/listeToConsulte'])
            },
            // s'il y a une erreur on renvoie un alert d'erreur
            () => {
              this.utilsService.showErrorMsg("Une erreur est seuvenue du serveur, veuillez réessayez plus tard");
            }
          )
        }
      }
    )
  }
}



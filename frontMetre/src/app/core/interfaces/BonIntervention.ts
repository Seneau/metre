export class BonIntervention{
    id?: number
    uoSecteur?: string
    residence?: string 
    nomClient?: string
	prenomClient?: string
    typePrestation?: string 
    referenceClient?: string
    referenceBi?: string
	dateDebut?: Date
	dateFin?: Date
	nomAgent?: string
	prenomAgent?: string
	adresse1?: string
	adresse2?: string
    presenceClient?: boolean
    presenceClientByMetreur?: boolean
    enginsPrevoir?: string
	complementAdresse?: string
    typeEspace?: string 
	commentaires?: string 
    autreCommentaires?: string 
    extension?: boolean 
    terassementClient?: boolean 
    protectionIncendie?: boolean
    separationRaccordement?: boolean
    droitSuite?: boolean
    climatisationRefroidissementEau?: boolean
    diametreBranchement?: number
    longueurBranchementCanalisation?: number
    longueurBranchementChausse?: number
    diametreCanalisationExistante?: number
    natureCanalisationExistante?: number 
    profondeurCanalisationExistante?: number
    codeService?: string 
    codetrocon?: string
    codeQuartier?: string
    codeTournee?: string
    rang?: number
    nombrePointEau?: number
    debitInstantanee?: number
    debitJournalier?: number
    debitHoraire?: number
    besoinDebitPression?: string 
    feuxTricolor?: boolean 
    circulationAlterne?: boolean 
    foncageChausse?: boolean 
    croisementOuvrage?: boolean 
    blindageTranche?: boolean 
    trotoir?: boolean 
    planMetre?: string
    accotement?: boolean
    empietementSansReduction?: boolean
    empietementAvecReduction?: boolean
    autresConsignes?: string
    tuyauDiam?: string
    colierPriseCharge?: string
    surCanalisationDiam?: string
    teDiam?: string
    robinetArretDiam?: string
    raccordCroix?: string
    teteBoucle?: string
    tubeAllonge?: string
    tabernacle?: string
    robinetAvantCompteurDiam?: string
    douillePurgeuseDiam?: string
    regardCompteur?: string
    compteurDiam?: string
    colCygneDiamn?: string
    coude?: string
    reduction? : string
    emboutCompteur?: string
    emboutRobinetArret?: string
    ecrouLaitonRobinetArret?: string
    ecrouLaitonCompteur?: string
    traverseMur?:number
    rocher?:number
    decoupageEnrobe?:number
    refectionEnrobe?:number
    gaineBuseTube?:number
    foncagePneumatique?: number
    sable?:number
    toutVenant?:number
    enrobe?:number
    croisementOuvrageNature1?: string
    croisementOuvrageNature2?: string
    abriCompteur?: string
    interuptionsDeservice?:number
    statut?: string
    motifAbandon?: string
}
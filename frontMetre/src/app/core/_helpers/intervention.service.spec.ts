import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { FormulaireComponent } from '../formulaire/formulaire.component';
import { BordereauService } from './bordereau.service';
import { InterventionService } from './intervention.service';
import { UtilsService } from './utils.service';


describe('Intervention Service', () => {
    let service: InterventionService;
    const http = jest.fn();


    beforeEach(() => {
        service = new InterventionService(http as any);
    });

//     describe('test 1',
//     () => {
//       it('should be created', () => {
//         expect((1+2)).toBe(3);
//       });
//     }
//   )

    describe('recup metre', () => {
        // const httpMock = jest.fn().mockReturnValue(of(response));
        it("get metre", () => {
            const response = { "id": 1, "uoSecteur": "update secteur" };
            const httpMock = {
                // on mock la reponse de la requette get
                get: jest.fn().mockReturnValue(of(response))
            };
            const mockService = new InterventionService(httpMock as any);
    
            const metreForm = new FormGroup({
                "id": new FormControl(1),
                "uoSecteur": new FormControl("update secteur")
            })
            mockService.getMetre(metreForm, 1).subscribe(
                (data) => {
                    expect(httpMock.get).toBeDefined();
                    expect(httpMock.get).toHaveBeenCalled();
                    // on espère la reponse d'etre égale à "response"
                    expect(data==response);
                }
            )
        })
    })
})
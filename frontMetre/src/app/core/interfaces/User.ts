export class User {
  firstName: string;
  lastName: string;
  email: string;
  name: string;
  matricule: string;
}

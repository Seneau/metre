import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { FormulaireComponent } from './formulaire/formulaire.component';
import { ListeMetreursComponent } from './liste-metreurs/liste-metreurs.component';
import { ListesToConsulteComponent } from './listes-to-consulte/listes-to-consulte.component';
import { PlanningComponent } from './planning/planning.component';
import { ReaffectationComponent } from './reaffectation/reaffectation.component';
import { AuthGuard } from './_helpers/auth.guard';


const routes: Routes = [
  {
    path:'',
    redirectTo:'accueil',
    pathMatch:'full'
  },
  {
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_METREUR','ROLE_RESPONSABLE_TECHNIQUE']},
    path:'accueil',
    component:AccueilComponent,
  },
  {
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_METREUR']},
    path:'planning',
    component:PlanningComponent,
  },
  {
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_METREUR','ROLE_BACK_OFFICE','ROLE_RESPONSABLE_TECHNIQUE']},
    path:'listeToConsulte/:id',
    component:FormulaireComponent,
  },
  {
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_METREUR','ROLE_BACK_OFFICE','ROLE_RESPONSABLE_TECHNIQUE']},
    path:'listeToConsulte',
    component:ListesToConsulteComponent,
  },
  {
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_RESPONSABLE_TECHNIQUE']},
    path:'liste-metreurs',
    component:ListeMetreursComponent,
  },
  {
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_RESPONSABLE_TECHNIQUE']},
    path:'reaffectation',
    component:ReaffectationComponent,
  }
  // {
  //   canActivate: [AuthGuard],
  //   data: { roles: ['ROLE_BACK_OFFICE']},
  //   path:'listeValidated',
  //   component:ListesValidatedComponent,
  // },
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }

import { Pipe, PipeTransform } from '@angular/core';
import { BonIntervention } from './interfaces/BonIntervention';

@Pipe({
  name: 'statutInput'
})
export class StatutInputPipe implements PipeTransform {

  transform(bi:BonIntervention[], statutInput: any): any[]{         
      
    if(!statutInput) {
      return  bi;
    }
    return bi.filter(
      x =>(x['statut'] as string).toLowerCase().includes(statutInput)
    )
    }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { KeycloakSecurityService } from './core/_helpers/keycloak-security.service';
import { SplashScreenService } from './shared/splash-screen/splash-screen.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Metre';
  private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

  constructor(
    private splashScreenService: SplashScreenService,
    private router: Router,
    private ks: KeycloakSecurityService) {
      if (ks.kc.authenticated) {
            if (ks.kc.hasRealmRole('ROLE_METREUR')) {
              router.navigate(['/']);
            } else if (ks.kc.hasRealmRole('ROLE_BACK_OFFICE')) {
              router.navigate(['/listeToConsulte']);

            }
      } else {
        ks.kc.login();
      }
  }

  ngOnInit() {
    const routerSubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // hide splash screen
        this.splashScreenService.hide();

        // scroll to top on every route change
        // window.scrollTo(10, 10);

        // to display back the body content
      //   setTimeout(() => {
      //     document.body.classList.add('page-loaded');
      //   }, 500);
      }
    });
    this.unsubscribe.push(routerSubscription);
  }
  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

}

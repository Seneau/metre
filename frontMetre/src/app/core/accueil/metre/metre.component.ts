import  frLocale  from '@fullcalendar/core/locales/fr';
import  esLocale  from '@fullcalendar/core/locales/es';
import { CalendarOptions } from '@fullcalendar/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AcceuilService } from '../../_helpers/acceuilService/acceuil.service';
import { UtilsService } from '../../_helpers/utils.service';
import { KeycloakSecurityService } from '../../_helpers/keycloak-security.service';
import { User } from '../../interfaces/User';

@Component({
  selector: 'app-metre',
  templateUrl: './metre.component.html',
  styleUrls: ['./metre.component.scss']
})
export class MetreComponent implements OnInit {
  closeModal: string
  isShown: boolean = false;
  @ViewChild('myInput') myInput: ElementRef<any>;
  @ViewChild('modalData') modalContent: any;

  user: User;

  constructor(
    private ks: KeycloakSecurityService,
    private utilsService: UtilsService,
    private acceuilService: AcceuilService,
    private router: Router,
    private modalService: NgbModal
  ){}

  taches;
  dateEvents = [];

  ngOnInit(){
    this.getUser();
  }

  calendarOptions: CalendarOptions = {
    eventColor: '#99BC47',
    initialView: 'dayGridMonth',
    headerToolbar: {
      start: 'prev,next today', 
      center: 'title', 
      end: 'dayGridDay,dayGridWeek,dayGridMonth' //droite
    },
    locale: 'fr',
    locales: [ esLocale, frLocale ],
  };

  getUser(){
    if (this.ks.kc.authenticated){
      this.ks.kc.loadUserInfo().then( (userData: User) => {
        this.user = userData;
        this.getBI(this.user["matricule"]);
      });
    }
  }

  //ici je récupére toutes les bon interventions du métreur connecté.......................
  //puis j'appelle la fonction boucle() expliqué ci-dessous...................
  getBI(matricule){
    this.utilsService.getBiByMatMetreur(matricule).subscribe(
      bi => {
        this.taches = bi,
        this.boucle(this.taches)
      }
    )
  }

  //Cette fonction parcour chaque bi et fait un push dans le tableau dateEvents[];
  //le tableau dateEvents[] est utilisé pour l'une des options de Full Calendar.
  boucle(array){
    array.forEach(element => {
      if(element.statut != "validé" && element.statut != "abandonné"){
        this.dateEvents.push({"reference_bi":element.referenceBi,"title":`${element.adresse1}`,"date":element.dateFin});
      }
    });
    this.calendarOptions.events = this.dateEvents;
  }
}

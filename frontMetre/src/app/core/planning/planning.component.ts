import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { PlanningService } from '../_helpers/planningService/planning.service';
// import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { KeycloakSecurityService } from '../_helpers/keycloak-security.service';
import { User } from '../interfaces/User';
import { UtilsService } from '../_helpers/utils.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss']
})

export class PlanningComponent implements OnInit {

  mock = environment.mockdataUrl;

  user: User;

  today;

  //all bi
  allBi;

  //Tâches
  allTaches = [];

  //day of week
  lundi;
  mardi;
  mercredi;
  jeudi;
  vendredi;
  samedi;
  dimanche;

  //day of the current week
  currentLundi;
  currentMardi;
  currentMercredi;
  currentJeudi;
  currentVendredi;
  currentSamedi;
  currentDimanche;

  //day of the next week
  nextLundi;
  nextMardi;
  nextMercredi;
  nextJeudi;
  nextVendredi;
  nextSamedi;
  nextDimanche;

  //Tache for the days of current week
  tacheForCurrentLundi=[];
  tacheForCurrentMardi=[];
  tacheForCurrentMercredi=[];
  tacheForCurrentJeudi=[];
  tacheForCurrentVendredi=[];
  tacheForCurrentSamedi=[];
  tacheForCurrentDimanche=[];

  //Tache for the days of Next week
  tacheForNextLundi=[];
  tacheForNextMardi=[];
  tacheForNextMercredi=[];
  tacheForNextJeudi=[];
  tacheForNextVendredi=[];
  tacheForNextSamedi=[];
  tacheForNextDimanche=[];

  nextAndPrevious:number = 0;

  constructor(
    private ks: KeycloakSecurityService,
    private planningService: PlanningService,
    private utilsService: UtilsService,
    private datePipe: DatePipe,
    private router: Router) { }

  ngOnInit(): void {
    this.week();
    this.getUser();
  }

  getUser(){
    if (this.ks.kc.authenticated){
      this.ks.kc.loadUserInfo().then( (userData: User) => {
        this.user = userData;
        this.getBI(this.user["matricule"]);
      });
    }
  }

  //ici je récupére toutes les bon interventions du métreur connecté.......................
  //puis j'appelle la fonction boucle() expliqué ci-dessous...................
  getBI(matricule){
    this.utilsService.getBiByMatMetreur(matricule).subscribe(
      bi => {
        this.allBi = bi,
        this.boucle(this.allBi)
      }
    )
  }

  //Cette fonction de boucle fait un push() de chaque bon-interventions dans un tableau spécifique..............
  //Si un bon-intervention n'est pas encore planifié, on le met dans le table allTaches[];
  //Si un bon-intervention a une date de planification, on le met dans son tableau spécifique
  //Par exemple si la date correspond au lundi on le met dans le tableau tacheForCurrentLundi[]...............
  boucle(array){
    array.forEach(element => {
      if(element.datePlanifie==null && element.statut=="en cours"){
        this.allTaches.push({"referenceBi":element.referenceBi,"dateFin":element.dateFin,"dateDebut":element.dateDebut,"datePlanifie":element.datePlanifie,"adresse1":element.adresse1,"id":element.id});
      }
      if(element.datePlanifie!=null  && element.statut!="abandonné"){
        //FOR DAYS OF CURRENT WEEK - FOR CURRENT DAYS OF WEEK - FOR CURRENT DAYS OF WEEK - FOR CURRENT DAYS OF WEEK
        if (element.datePlanifie == this.datePipe.transform(this.currentLundi, 'yyyy-MM-dd')){
          this.tacheForCurrentLundi.push(element); 
        }
        if (element.datePlanifie == this.datePipe.transform(this.currentMardi, 'yyyy-MM-dd')){
          this.tacheForCurrentMardi.push(element);          
        }
        if (element.datePlanifie == this.datePipe.transform(this.currentMercredi, 'yyyy-MM-dd')){
          this.tacheForCurrentMercredi.push(element);          
        }
        if (element.datePlanifie == this.datePipe.transform(this.currentJeudi, 'yyyy-MM-dd')){
          this.tacheForCurrentJeudi.push(element);          
        }
        if (element.datePlanifie == this.datePipe.transform(this.currentVendredi, 'yyyy-MM-dd')){
          this.tacheForCurrentVendredi.push(element);          
        }
        if (element.datePlanifie == this.datePipe.transform(this.currentSamedi, 'yyyy-MM-dd')){
          this.tacheForCurrentSamedi.push(element);          
        }
        if (element.datePlanifie == this.datePipe.transform(this.currentDimanche, 'yyyy-MM-dd')){
          this.tacheForCurrentDimanche.push(element);          
        }
        //FOR DAYS OF NEXT WEEK - FOR DAYS OF NEXT WEEK - FOR DAYS OF NEXT WEEK - FOR DAYS OF NEXT WEEK - FOR DAYS OF NEXT WEEK
        if (element.datePlanifie == this.datePipe.transform(this.nextLundi, 'yyyy-MM-dd')){
          this.tacheForNextLundi.push(element);
        }
        if (element.datePlanifie == this.datePipe.transform(this.nextMardi, 'yyyy-MM-dd')){
          this.tacheForNextMardi.push(element);          
        }
        if (element.datePlanifie == this.datePipe.transform(this.nextMercredi, 'yyyy-MM-dd')){
          this.tacheForNextMercredi.push(element);          
        }
        if (element.datePlanifie == this.datePipe.transform(this.nextJeudi, 'yyyy-MM-dd')){
          this.tacheForNextJeudi.push(element);          
        }
        if (element.datePlanifie == this.datePipe.transform(this.nextVendredi, 'yyyy-MM-dd')){
          this.tacheForNextVendredi.push(element);          
        }
        if (element.datePlanifie == this.datePipe.transform(this.nextSamedi, 'yyyy-MM-dd')){
          this.tacheForNextSamedi.push(element);          
        }
        if (element.datePlanifie == this.datePipe.transform(this.nextDimanche, 'yyyy-MM-dd')){
          this.tacheForNextDimanche.push(element);          
        }
      }
    });
  }

  //Cette fonction détermine la date des jours de la semaine actuelle.................
  week(actuelleDate: Date = new Date()){
      this.nextAndPrevious = 7;
      this.currentDimanche = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()+7);
      this.currentLundi = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()-6);
      this.currentMardi = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()+2);
      this.currentMercredi = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()+3);
      this.currentJeudi = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()+4);
      this.currentVendredi = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()+5);
      this.currentSamedi = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()+6);

      //DAYS FOX NEXT WEEK
      this.nextDimanche = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()+this.nextAndPrevious+7);
      this.nextLundi = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()-6);
      this.nextMardi = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()+2);
      this.nextMercredi = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()+3);
      this.nextJeudi = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()+4);
      this.nextVendredi = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()+5);
      this.nextSamedi = actuelleDate.setUTCDate(actuelleDate.getUTCDate()-actuelleDate.getUTCDay()+6);
    
  }

  nextAndPreviousWeek(){
    if(this.nextAndPrevious == 7){
      this.nextAndPrevious = this.nextAndPrevious+7
      return this.nextAndPrevious;
    }
    return this.week();
  }



  //Cette fonction réactualise le component actuellement chargé.........................
  reloadComponent() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }

  //Cette fonction fait une comparaison de la date de plannification (date1) et de la date limite (date2)
  // où une tache doit être réaliser.
  //Si la (date1) est supérieur à (date2) elle te lance une alert puis, après confirmation, la fonction
  //change la valeur de la date1 à null puis le remet sur le tableau allTaches[] en rechargant la page.
  //Si la (date1) est inférieur à (date3) elle te lance une alert puis, après confirmation, la fonction
  //change la valeur de la date1 à null puis le remet sur le tableau allTaches[] en rechargant la page.
  //Sinon on fait un update du bon-intervention avec la nouvelle date plannifiée..............................
  dateComparing(date1, date2, date3, id, toUpdate){
    if(date1>date2){
      return Swal.fire({
        title: "Attention!!! La date de planification ne doit pas dépasser la date limite.",
        confirmButtonColor: '#203A73',
        confirmButtonText: 'ok'
      }).then(
        (result) => {
          if (result.isConfirmed) {
            this.planningService.getById(id).subscribe(
              bon => {
                bon["datePlanifie"] = null;
                this.put(bon),
                this.reloadComponent()
              }
            )
          }
        }
      )
    }
    if(date1<date3){
      date3 = this.datePipe.transform(new Date(), 'EEEE, d, MMMM');
      return Swal.fire({
        title: `Attention!!! on est le ${date3}. La date de plannification doit dépasser la date actuelle ou être égale à elle.`,
        confirmButtonColor: '#203A73',
        confirmButtonText: 'ok'
      }).then(
        (result) => {
          if (result.isConfirmed) {
            this.planningService.getById(id).subscribe(
              bon => {
                bon["datePlanifie"] = null;
                this.put(bon),
                this.reloadComponent()
              }
            )
          }
        }
      )
    }
    return this.put(toUpdate);
  }

  //Cette fonction fait un update d'un élément
  put(elmnt){
    if(elmnt){
      return this.planningService.putPrestation(elmnt).subscribe(
        putting => {
          console.log(putting);
        }
      )
    }
  }

  //Cette fonction est appelée au niveau de la fonction drop()
  //Elle a pour rôle d'apdater la date de planification avec la date du tableau ou on a fait un drop.
  //Par exemple si l'élément est mis sur le tableau tacheForCurrentLundi[], on parcour d'abord le tableau puis
  //on prend chaque élément placé sur le tableau, on change sa date de planification avec la valeur de
  //la date du tableau tacheForCurrentLundi[].
  //Après on fait un getById() de l'élément by son Id et enfin on fait put()......................
  //NB: la date du tableau tacheForCurrentLundi[] est this.currentLundi..................
  onDrop(){
    this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

    if(this.allTaches.length!=null){
      for (let index = 0; index < this.allTaches.length; index++) {
        const element= this.allTaches[index];
        element.datePlanifie = null;
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = null;
            this.put(bon)
          }
        )
      }
    }
    //FOR DAYS OF CURRENT WEEK - FOR CURRENT DAYS OF WEEK - FOR CURRENT DAYS OF WEEK - FOR CURRENT DAYS OF WEEK
    if(this.tacheForCurrentLundi.length){
      for (let index = 0; index < this.tacheForCurrentLundi.length; index++) {
        const element= this.tacheForCurrentLundi[index];
        element.datePlanifie = this.datePipe.transform(this.currentLundi, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.currentLundi, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        );
      }
    }
    if(this.tacheForCurrentMardi.length){
      for (let index = 0; index < this.tacheForCurrentMardi.length; index++) {
        const element= this.tacheForCurrentMardi[index];
        element.datePlanifie = this.datePipe.transform(this.currentMardi, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.currentMardi, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        )
      }
    }
    if(this.tacheForCurrentMercredi.length){
      for (let index = 0; index < this.tacheForCurrentMercredi.length; index++) {
        const element= this.tacheForCurrentMercredi[index];
        element.datePlanifie = this.datePipe.transform(this.currentMercredi, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.currentMercredi, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        )
      }
    }
    if(this.tacheForCurrentJeudi.length){
      for (let index = 0; index < this.tacheForCurrentJeudi.length; index++) {
        const element= this.tacheForCurrentJeudi[index];
        element.datePlanifie = this.datePipe.transform(this.currentJeudi, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.currentJeudi, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        )
      }
    }
    if(this.tacheForCurrentVendredi.length){
      for (let index = 0; index < this.tacheForCurrentVendredi.length; index++) {
        const element= this.tacheForCurrentVendredi[index];
        element.datePlanifie = this.datePipe.transform(this.currentVendredi, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.currentVendredi, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        )
      }
    }
    if(this.tacheForCurrentSamedi.length){
      for (let index = 0; index < this.tacheForCurrentSamedi.length; index++) {
        const element= this.tacheForCurrentSamedi[index];
        element.datePlanifie = this.datePipe.transform(this.currentSamedi, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.currentSamedi, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        )
      }
    }
    if(this.tacheForCurrentDimanche.length){
      for (let index = 0; index < this.tacheForCurrentDimanche.length; index++) {
        const element= this.tacheForCurrentDimanche[index];
        element.datePlanifie = this.datePipe.transform(this.currentDimanche, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.currentDimanche, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        )
      }
    }

    //FOR DAYS OF NEXT WEEK - FOR DAYS OF NEXT WEEK - FOR DAYS OF NEXT WEEK - FOR DAYS OF NEXT WEEK - FOR DAYS OF NEXT WEEK

    if(this.tacheForNextLundi.length){
      for (let index = 0; index < this.tacheForNextLundi.length; index++) {
        const element= this.tacheForNextLundi[index];
        element.datePlanifie = this.datePipe.transform(this.nextLundi, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.nextLundi, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        );
      }
    }
    if(this.tacheForNextMardi.length){
      for (let index = 0; index < this.tacheForNextMardi.length; index++) {
        const element= this.tacheForNextMardi[index];
        element.datePlanifie = this.datePipe.transform(this.nextMardi, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.nextMardi, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        )
      }
    }
    if(this.tacheForNextMercredi.length){
      for (let index = 0; index < this.tacheForNextMercredi.length; index++) {
        const element= this.tacheForNextMercredi[index];
        element.datePlanifie = this.datePipe.transform(this.nextMercredi, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.nextMercredi, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        )
      }
    }
    if(this.tacheForNextJeudi.length){
      for (let index = 0; index < this.tacheForNextJeudi.length; index++) {
        const element= this.tacheForNextJeudi[index];
        element.datePlanifie = this.datePipe.transform(this.nextJeudi, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.nextJeudi, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        )
      }
    }
    if(this.tacheForNextVendredi.length){
      for (let index = 0; index < this.tacheForNextVendredi.length; index++) {
        const element= this.tacheForNextVendredi[index];
        element.datePlanifie = this.datePipe.transform(this.nextVendredi, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.nextVendredi, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        )
      }
    }
    if(this.tacheForNextSamedi.length){
      for (let index = 0; index < this.tacheForNextSamedi.length; index++) {
        const element= this.tacheForNextSamedi[index];
        element.datePlanifie = this.datePipe.transform(this.nextSamedi, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.nextSamedi, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        )
      }
    }
    if(this.tacheForNextDimanche.length){
      for (let index = 0; index < this.tacheForNextDimanche.length; index++) {
        const element= this.tacheForNextDimanche[index];
        element.datePlanifie = this.datePipe.transform(this.nextDimanche, 'yyyy-MM-dd');
        this.planningService.getById(element.id).subscribe(
          bon => {
            bon["datePlanifie"] = this.datePipe.transform(this.nextDimanche, 'yyyy-MM-dd'),
            this.dateComparing(element.datePlanifie, element.dateFin, this.today, element.id, bon)
          }
        )
      }
    }

  }

  //DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP 

  //Cette fonction est la fonction de drop pour déplacer les éléments dans les tableaux.
  //Elle appelle la fonction onDrop() pour faire les traitements spécifique à chaque drop................................
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
    this.onDrop();
  }

  //OBSOLÉTE OBSOLÉTE OBSOLÉTE OBSOLÉTE OBSOLÉTE OBSOLÉTE OBSOLÉTE OBSOLÉTE OBSOLÉTE OBSOLÉTE OBSOLÉTE OBSOLÉTE

  //ici je récupére toutes les bon interventions grâce aux Mock Data..........................
  // getBI(){
  //   return this.http.get(this.mock+"/tache").subscribe(
  //     (bi:any) => {
  //       this.allBi = bi,
  //       console.log(this.allBi),
  //       this.boucle(this.allBi)
  //     }
  //   )
  // }

  // boucle(array){
  //   for (let index = 0; index < array.length; index++) {
  //     const element= array[index];
  //     this.tachePlannifed = element;
  //     console.log(this.tachePlannifed);
  //   }
  // }

}

import { TestBed } from '@angular/core/testing';

import { AcceuilRtService } from './acceuil-rt.service';

describe('AcceuilRtService', () => {
  let service: AcceuilRtService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AcceuilRtService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

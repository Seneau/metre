import { KeycloakSecurityService } from './../_helpers/keycloak-security.service';
import { KeycloakInstance } from 'keycloak-js';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilsService } from '../_helpers/utils.service';

@Component({
  selector: 'app-listes-to-consulte',
  templateUrl: './listes-to-consulte.component.html',
  styleUrls: ['./listes-to-consulte.component.scss']
})
export class ListesToConsulteComponent implements OnInit {

  role: string;
  kc: KeycloakInstance;
  
  constructor(private ks: KeycloakSecurityService,private http: HttpClient, private router: Router, private utilesService: UtilsService) {
    this.kc = ks.kc;
  }

  ngOnInit(): void {
    this.getRole();
  }

  getRole(){
    return this.role = this.ks.getCurrentRoleUsercomplet();
  }

}

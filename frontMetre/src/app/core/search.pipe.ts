import { Input, Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {
  
  transform(users:[], searchInput: string): any[]{ 
    if(!searchInput) {
        return  users;
    }
    searchInput = searchInput.toLowerCase();
       return users.filter(
           x =>((x['referenceBi'] as string).toLowerCase().includes(searchInput) 
           || 
           (x['nomClient'] as string).toLowerCase().includes(searchInput)
           || 
           (x['prenomClient'] as string).toLowerCase().includes(searchInput)
           || 
           (x['adresse1'] as string).toLowerCase().includes(searchInput)
           )
       )
     }
}

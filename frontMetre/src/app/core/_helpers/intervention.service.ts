import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { BonIntervention } from '../interfaces/BonIntervention';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class InterventionService {

  constructor(private http: HttpClient) { }

  // tableau des formControl autres que checkbox
  keys = ["enginsPrevoir",
    "besoinDebitPression",
    "autreCommentaires",
    "diametreBranchement",
    "longueurBranchementCanalisation",
    "longueurBranchementChausse",
    "diametreCanalisationExistante",
    "natureCanalisationExistante",
    "profondeurCanalisationExistante",
    "codeService",
    "codetrocon", "codeQuartier",
    "codeTournee",
    "rang",
    "nombrePointEau",
    "debitInstantanee",
    "debitJournalier",
    "debitHoraire",
    "autresConsignes"
  ];

  // tableau des formControl Checkbox
  checkKeys = [
    "droitSuite",
    "climatisationRefroidissementEau",
    "terassementClient",
    "protectionIncendie",
    "separationRaccordement",
    "extension",
    "presenceClientByMetreur",
    "feuxTricolor",
    "circulationAlterne",
    "foncageChausse",
    "croisementOuvrage",
    "blindageTranche",
    "trotoir",
    "accotement",
    "empietementSansReduction",
    "empietementAvecReduction",
  ];



  // fonction permettant de récuperer un metré
  getMetre(form: FormGroup, id: number): Observable<BonIntervention> {
    return this.http.get<BonIntervention>(environment.apiUrl + '/api/boninterventions/' + id).pipe(
      map((intervention: BonIntervention) => {
        // les attributs à charger
        const unchangedDatas = ["uoSecteur", "residence", "nomClient", "prenomClient", "typePrestation", "referenceClient", "referenceBi", "dateDebut", "dateFin", "nomAgent", "prenomAgent", "adresse1", "adresse2", "presenceClient", "complementAdresse", "typeEspace", "commentaires"];
        unchangedDatas.forEach(unchangedData => {
          form.get(unchangedData).setValue(intervention[unchangedData].toString());
        })

        // pour chaque form control du formulaire on le met comme value sa correspondance dans la base de données
        this.keys.forEach(key => {
          if(intervention[key]){
            if(form.get(key)){
            form.get(key).setValue(intervention[key]);
            }
          }
        })

        // de meme pour les checkbox
        this.checkKeys.forEach(key => {
          if(intervention[key]){
            form.get(key).setValue(intervention[key].toString());
          }
        })
        return intervention;
      }))
  }

  // fonction permettant de mettre à jour un métré
  updateMetre(form: FormGroup, intervention: BonIntervention) {
    // on met à jour les valeurs du BI étant des inputs
    this.keys.forEach(element => {
      if (intervention[element] != form.get(element).value) {
        intervention[element] = form.get(element).value
      }
    });
    // on met à jour les checkbox
    this.checkKeys.forEach(element => {
      if (intervention[element] != JSON.parse(form.get(element).value)) {
        intervention[element] = JSON.parse(form.get(element).value)
      }
    });
    intervention.statut = "envoyé";
    // on envoie la requette du update
    return this.http.put<any>(environment.apiUrl + '/api/bon-interventions/', intervention);
  }

  // fonction permettant de désactiver les inputs si le user connecté est un:
  // backoffice, un responsable technique ou si le statut du bi est "envoyé"
  ActivateViewOnly(userRole: string, form: FormGroup,intervention: BonIntervention){
    // BACK_OFFICE =
    if(userRole == "BACK_OFFICE" || userRole == "RESPONSABLE_TECHNIQUE" || intervention.statut == 'envoyé'){
      this.keys.forEach(key => {
        form.controls[key].disable();
      });
      this.checkKeys.forEach(key => {
        form.controls[key].disable();
      });
    }
  }

  // fonction pour mettre à jour un métré abandonné
  adandonMetre(intervention: BonIntervention, motif: string): Observable<BonIntervention>{
    // on modifie le motif du BI
    intervention.motifAbandon = motif;
    // on met le statut du BI à "abandonné"
    intervention.statut = "abandonné";
    return this.http.put<BonIntervention>(environment.apiUrl + '/api/bon-interventions/', intervention);
  }

  // fonction permettant de valider un métré coté backoffice
  validateMetre(intervention: BonIntervention){
    // on met le statut du BI à validé
    intervention.statut = "validé";
    // on met le motif à null (facultatif)
    intervention.motifAbandon = null;
    return this.http.put<BonIntervention>(environment.apiUrl + '/api/bon-interventions/', intervention);
  }
}

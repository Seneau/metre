import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
// import { environment } from 'src/environments/environment';
import { UtilsService } from '../_helpers/utils.service';

@Component({
  selector: 'app-listes-validated',
  templateUrl: './listes-validated.component.html',
  styleUrls: ['./listes-validated.component.scss']
})
export class ListesValidatedComponent implements OnInit {

  mockUrl = environment.mockdataUrl;
  liste;
  searchInput: string;
  
  constructor(private http: HttpClient, private router: Router, private utilesService: UtilsService) { }

  ngOnInit(): void {
    this.utilesService.getListeToConsules().subscribe(
      (listeForm) => {
        this.liste = listeForm;
        console.log(this.liste);
      }
    )
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
// import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlanningService {

  private host = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAllPrestations(){
    return this.http.get(`${this.host}/api/bon-interventions`);
  }

  putPrestation(data){
    return this.http.put(`${this.host}/api/bon-interventions`,data);
  }

  getById(id){
    return this.http.get(`${this.host}/api/boninterventions/${id}`);
  }

}

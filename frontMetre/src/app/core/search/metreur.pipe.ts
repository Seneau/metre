import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'metreur'
})
export class MetreurPipe implements PipeTransform {

  transform(users:[], searchMetreurInput: string): any[]{ 
    if(!searchMetreurInput) {
        return  users;
    }
    searchMetreurInput = searchMetreurInput.toLowerCase();
       return users.filter(
           x =>(
           (x['firstName'] as string).toLowerCase().includes(searchMetreurInput)
           || 
           (x['lastName'] as string).toLowerCase().includes(searchMetreurInput)
           || 
           (x['attributes["matricule"]'] as string).toLowerCase().includes(searchMetreurInput)
           || 
           (x['attributes["zone"]'] as string).toLowerCase().includes(searchMetreurInput)
           )
       )
     }

}

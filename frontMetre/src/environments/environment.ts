// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  mockdataUrl: "http://localhost:3000/",
  apiUrl: "http://localhost:9195",
  // serveur AWS
  // apiUrl: "http://18.117.79.162:9195/",

  keycloak: {

    // issuer: 'http://localhost:8080/auth/',
    issuer: 'https://localhost/auth/',
    // issuer: 'http://3.17.14.157/auth/',


    realm: 'demo-angular',
    // realm: 'seneau',


    clientId: 'demo-angular',
    // clientId: 'metre_front',


  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AcceuilRtService } from '../_helpers/acceuilService/acceuil-rt/acceuil-rt.service';
import { PlanningService } from '../_helpers/planningService/planning.service';
import { UtilsService } from '../_helpers/utils.service';

@Component({
  selector: 'app-reaffectation',
  templateUrl: './reaffectation.component.html',
  styleUrls: ['./reaffectation.component.scss']
})
export class ReaffectationComponent implements OnInit {
  
  // mock = environment.mockdataUrl;

  //On stockera dedans toutes les prestations (bons intervention)
  allPrestation;

  //Ce tableau va recevoir tous les métreurs du RT (responsable technique) connecté
  metreur = [];

  //Ce tableau va recevoir une collection de tableaux
  //Chaque métreur aura un tableau différent
  tab: any[]=[];

  //Ce tableau recevra tous les bi (bons intervention) avec le statut "abandonné" 
  allBiAbandonned = [];

  //Ce tableau recevra les matricules du rt connecté
  matricules = [];

  constructor(private planningService: PlanningService,
    private router: Router,
    private serviceRt : AcceuilRtService,
    private utilsService: UtilsService) { }

  ngOnInit(): void {
    this.getMetreurs();
  }

  //Cette méthode recupére tous les métreurs du RT connecté
  getMetreurs(){
    this.serviceRt.getAllUsers().subscribe(
      users => {
        this.metreur = this.utilsService.getMetreurByMatriculeRT(users, 121217);
        this.addTab(this.metreur);
        this.boucleToGetMat(this.metreur);
      }
    );
  }

  //Cette méthode cré des tableaux dans le tableau tab[]
  //Chaque tableau aura un nom spécifique pour chaque métreur
  addTab(i) {
    for (let index = 0; index < i.length; index++) {
      const element = i[index];
      this.tab['cdk-drop-list-' + element.attributes["matricule"][0]] = new Array();
    }
  }

  //Ici on fait un push du matricule des métreurs dans le tableau matricules[]
  boucleToGetMat(array){
    array.forEach(index => {
      const element = index;
      this.matricules.push(element["attributes"]["matricule"][0]);
    });
    this.getPrestations();
  }

  //Ici on récupére toutes les bon interventions.......................
  getPrestations(){
    this.matricules.forEach(index => {
      const element = index;
      this.utilsService.getBiByMatMetreur(element).subscribe(
        (prestations) => {
          this.allPrestation = prestations,
          this.boucle(this.allPrestation)
        }
      );
    });
  }

  //Cette fonction de boucle fait un push() de chaque bon-interventions avec le statut "abandonné" dans le tableau allBiAbandonned[]..............
  //Elle fait aussi un push() de chaque bon-interventions avec le statut "en cours" dans le tableau correspondant à un métré spécifiquement
  boucle(array){
    array.forEach(element => {
      // if(element.statut=="abandonné"){
      //   this.allBiAbandonned.push({element:{"referenceBi":element.referenceBi,"dateFin":element.dateFin,"adresse1":element.adresse1,"datePlanifie":element.datePlanifie,"id":element.id, "matricule":element.matricule, "statut":element.statut}});
      // }
      if(element.statut=="en cours"){
        this.tab['cdk-drop-list-' + element.matricule].push({element});
      }
    });
  }

  //Cette fonction réactualise le component actuellement chargé.........................
  reloadComponent() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }

  //Cette fonction fait un update d'un élément
  put(elmnt){
    if(elmnt){
      return this.planningService.putPrestation(elmnt).subscribe(
        putting => {
          console.log(putting);
        }
      )
    }
  }

  //Cette fonction parcours chaque tableau existant.. et vérifie si le tableau a de nouvelle données ajouté
  //Puis elle fait les traitements nécessaire pour chaque tableau
  //Elle reçoit en paramétre les données d'un métreur et le tableau du métreur correspondant.................................
  onDrop(metreur,tab){
    // if(this.allBiAbandonned.length){
    //   this.allBiAbandonned.forEach(index => {
    //     const element= index["element"];
    //     // console.log(element);
    //     if(element){
    //       this.planningService.getById(element["id"]).subscribe(
    //         bon => {
    //             bon["datePlanifie"] = null;
    //             bon["statut"] = "abandonné";
    //             this.put(bon);
    //             // console.log(bon);
    //         }
    //       )
    //     }
    //   });
    // }

    if(this.tab[tab].length){
      this.tab[tab].forEach(index => {
        const element= index["element"];
        if(element){
          this.planningService.getById(element["id"]).subscribe(
            bon => {
              if(bon["statut"] != "en cours"){
                bon["datePlanifie"] = null;
                bon["motifAbandon"] = null;
                bon["matricule"] = metreur.attributes['matricule'][0];
                bon["prenomAgent"] = metreur.firstName;
                bon["nomAgent"] = metreur.lastName;
                bon["statut"] = "en cours";
                this.put(bon);
              }else if(bon["statut"] == "en cours" && bon["matricule"] != metreur.attributes['matricule'][0]){
                bon["datePlanifie"] = null;
                bon["matricule"] = metreur.attributes['matricule'][0];
                bon["prenomAgent"] = metreur.firstName;
                bon["nomAgent"] = metreur.lastName;
                bon["statut"] = "en cours";
                this.put(bon);
              };
              // console.log(bon);
            }
          )
        }
      });
    }
  }

  //DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP DROP 

  //Cette fonction est la fonction de drop pour déplacer les éléments dans les tableaux.
  //Elle fait un appelle à la fonction onDrop() pour faire les traitements spécifique à chaque drop................................
  drop(event: CdkDragDrop<string[]>,metreur,tab) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
    this.onDrop(metreur,tab);
  }

  // //ici je récupére toutes les bon interventions grâce aux Mock Data..........................
  // getPrestations(){
  //   return this.http.get(this.mock+"interventions").subscribe(
  //     (prestations:any) => {
  //       this.allPrestation = prestations,
  //       this.boucle(this.allPrestation)
  //     }
  //   )
  // }

}

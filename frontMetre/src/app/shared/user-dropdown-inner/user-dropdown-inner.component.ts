import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../core/interfaces/User';
import { KeycloakSecurityService } from '../../core/_helpers/keycloak-security.service';
// import { KeycloakSecurityService } from 'src/app/core/_helpers/keycloak-security.service';
// import { User } from 'src/app/core/interfaces/User';
@Component({
  selector: 'app-user-dropdown-inner',
  templateUrl: './user-dropdown-inner.component.html',
  styleUrls: ['./user-dropdown-inner.component.scss'],
})
export class UserDropdownInnerComponent implements OnInit {
  extrasUserDropdownStyle: 'light' | 'dark' = 'light';
  user: User;
  roleUser = '';


  constructor(private ks: KeycloakSecurityService) {
    if (ks.kc.authenticated) {

      ks.kc.loadUserProfile().then( (data: User) => {
        this.user = data;
        console.log(data);
      });
    }
  }

  ngOnInit(): void {

  }

  logout() {
    this.ks.kc.logout();
    // document.location.reload();
  }
  profil(){
    this.ks.kc.accountManagement();
  }
}

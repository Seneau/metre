import { UtilsService } from './../_helpers/utils.service';
import { Component, OnInit } from '@angular/core';
import { AcceuilRtService } from '../_helpers/acceuilService/acceuil-rt/acceuil-rt.service';

@Component({
  selector: 'app-liste-metreurs',
  templateUrl: './liste-metreurs.component.html',
  styleUrls: ['./liste-metreurs.component.scss']
})
export class ListeMetreursComponent implements OnInit {

  metreur;
  searchMetreurInput: string;

  constructor(private serviceRt : AcceuilRtService, private utilsService: UtilsService) { }

  ngOnInit(): void {
    this.serviceRt.getAllUsers().subscribe(
      users => {
        console.log(users);
        this.metreur = this.utilsService.getMetreurByMatriculeRT(users, 121217);
        console.log(this.metreur);
      }
    );
  }

}

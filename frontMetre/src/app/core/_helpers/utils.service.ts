// import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BonIntervention } from '../interfaces/BonIntervention';
import Swal from 'sweetalert2'
import { environment } from '../../../environments/environment';
import { events } from '../interfaces/Event';


@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  constructor(private http: HttpClient) { 

  }

  getEvents(): Observable<events[]> {
    return this.http.get<events[]>(environment.apiUrl+'events');
  }

  // recuperer la liste des formulaires à consulter
  getListeToConsules(): Observable<BonIntervention[]>{
    return this.http.get<BonIntervention[]>(environment.apiUrl+"/api/bon-interventions");
  }

  //on recupére les bi d'un metreur grace à son matricule
  getBiByMatMetreur(matricule): Observable<BonIntervention[]>{
    return this.http.get<BonIntervention[]>(environment.apiUrl+`/api/bon-interventions/${matricule}`);
  }
  
  // fonction pour l'affichage d'un msg de succès
  showSuccesMsg(msg: string){
    return Swal.fire({
      title: "Traitement réussi",
      icon: 'success',
      text: msg,
      showConfirmButton: false,
      timer: 1500
    })
  }

  // fonction pour l'affichage d'un msg d'ereur
  showErrorMsg(msg: string){
    return Swal.fire({
      title: "<div style='color: #F14D60;'>Erreur</div>",
      text: msg,
      showConfirmButton: false,
      background: '',
      customClass: {container: 'customClassText', htmlContainer: 'customClassText', popup: 'customClassText'},
      timer: 3000
    })
  }
  // fonction pour l'affichage d'un msg de confirmation
  showConfrimationMsg(msg: string){
    return Swal.fire({
      title: "Confirmation",
      text: msg,
      showCancelButton: true,
      confirmButtonColor: '#203A73',
      cancelButtonColor: '#99BC47',
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    })
  }

  //on recupére les métreurs grace au matricule du rt
  getMetreurByMatriculeRT(allAgents: [], matricule_rt){
   const agents = [];
   allAgents.filter( agent =>{
      if( agent['attributes'] && agent['attributes']['matricule_rt'] && agent['attributes']['matricule_rt'][0]===`${matricule_rt}`){
         agents.push(agent)
      }
    })
    return agents;
  }

  sum(n: number, m: number){
    return n+m;
  }

}

import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { UtilsService } from './utils.service';


describe('UtilsService', () => {
  let service: UtilsService;
  // mock http
  const http = jest.fn();

  // const http = new HttpClient(null);

  beforeEach(() => {
    service = new UtilsService(http as any);
  });
 
  describe('test 1',
    () => {
      it('should be created', () => {
        expect((service.sum(1, 2))).toBe(3);
      });
    }
  )

  // test de la fonction getEvents()
  describe('test 2', () => {
    it('it should get events', () => {
      const response = [
      {"id": 1, "title": "titre1", "date": "2021-07_13"},
      {"id": 1, "title": "titre1", "date": "2021-07_13"},
      ]
      const httpMOck = {
        // on mock la reponse de la requette get
        get: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new UtilsService(httpMOck as any);
      serviceMock.getEvents().subscribe((data) => {
        expect(httpMOck.get).toBeDefined();
        expect(httpMOck.get).toHaveBeenCalled();
        // on espère la reponse d'etre égale à "response"
        expect(data==response);
      })
    });
  }


)

 
});
